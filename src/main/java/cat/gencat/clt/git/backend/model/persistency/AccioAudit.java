package cat.gencat.clt.git.backend.model.persistency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "GITTBAUDIACC")
public class AccioAudit {

	@Id
	@Column(name = "AUDIACC_IDACCIO")
	private Long id;
	
	@Column(name = "AUDIACC_NOMACCIO")
    private String nom;
    
}
