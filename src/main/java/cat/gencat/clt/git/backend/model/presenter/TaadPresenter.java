package cat.gencat.clt.git.backend.model.presenter;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class TaadPresenter {

	@NotNull
	private Long id;
	
	@NotNull
	private String codi;
	
    private String serie;
    
    private int ambit1;
    
    private int ambit2;
    
    private String funcAdm;
    
    private String marcLegal;
    
    private String documents;
    
    private String documentsRecap;
    
    private int disposicio;
    
    private int terminiDisposicio1;
    
    private String terminiAplicacio;
    
    private int terminiDisposicio2;
    
    private int terminiVigencia;
    
    private int valorAdm;
    
    private int valorLegal;
    
    private int informatiu;
    
    private int regim;
    
    private int motivacio;
    
    private int vigencia;
    
    private LocalDate dataInici;
    
    private LocalDate dataFi;
    
    private String fomanJuridica;
    
    private int organisme;
    
    private String ordre;
    
    private String dogc;
    
    private int situacio;
    
    private String seriesRel;
    
    private String seriesAnt;
    
    private String observacions;
    
    private String altreFonJur;
    
    private LocalDate pritraPublicacio;
    
    private LocalDate dartraPublicacio;
    
}
