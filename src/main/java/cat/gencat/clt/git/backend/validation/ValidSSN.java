package cat.gencat.clt.git.backend.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target(TYPE)
@Retention(RUNTIME)
@Constraint(validatedBy = SSNConstraintValidator.class)
public @interface ValidSSN {

    String message() default "{cat.gencat.clt.git.backend.validation.ssn.notnull.message}";

	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };
}
