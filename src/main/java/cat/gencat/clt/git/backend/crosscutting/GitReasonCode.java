package cat.gencat.clt.git.backend.crosscutting;

public enum GitReasonCode {

    INTERNAL,
    VALIDATION_FAILED,
    QCDF_NOT_UPDATABLE
    
}
