package cat.gencat.clt.git.backend.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;

@Configuration
@EnableWebFluxSecurity
@Profile({"loc"})
// @EnableGlobalMethodSecurity(prePostEnabled = true)
// @EnableReactiveMethodSecurity...
public class LocalSecurityConfiguration {

    @Bean
	SecurityWebFilterChain springSecurityFilterChain(
        ServerHttpSecurity http
    ) {
        final CorsConfigurationSource configurationSource = serverWebExchange -> {
            final var cc = new CorsConfiguration();
            cc.addAllowedOrigin("*");
            cc.addAllowedMethod("*");
            cc.addAllowedHeader("*");

            return cc;
        };

        return http
            .cors().configurationSource(configurationSource).and()
            .csrf().disable()
            .authorizeExchange((authorizeSpec) -> authorizeSpec.anyExchange().permitAll())
            .build();
    }

}
