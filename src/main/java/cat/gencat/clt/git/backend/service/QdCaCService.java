package cat.gencat.clt.git.backend.service;

import java.time.LocalDate;
import java.util.Objects;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import cat.gencat.clt.git.backend.mapper.PresenterMapper;
import cat.gencat.clt.git.backend.model.persistency.QdCaC;
import cat.gencat.clt.git.backend.model.presenter.PageableResponseModel;
import cat.gencat.clt.git.backend.model.presenter.QdCaCPresenter;
import cat.gencat.clt.git.backend.model.presenter.QdCaCSearchFormModel;
import cat.gencat.clt.git.backend.repositories.QdCaCRepository;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class QdCaCService {

	private final PresenterMapper presenterMapper;
	private final QdCaCRepository qdcacRepository;

	public Flux<QdCaCPresenter> getQdCaCs() {
		return this.qdcacRepository.findAll()
			.map(this.presenterMapper::toPresenter);
	}
	
	// public Mono<PageableResponseModel<QdCaCPresenter>> getQdCaCSearchQueryDsl(QdCaCSearchFormModel form, Pageable pageable) {
	// 	Function<List<QdCaCPresenter>, PageableResponseModel<QdCaCPresenter>> toResponse =
	// 		(items) -> PageableResponseModel.<QdCaCPresenter>builder().size(items.size()).page(form.getPage()).results(items).build();

	// 	return this.qdcacRepository.findAll(this.queryMapper.toPredicate(form))
	// 		.map(this.presenterMapper::toPresenter)
	// 		.collectList()
	// 		.map(toResponse);
	// }

	public Mono<PageableResponseModel<QdCaCPresenter>> getQdCaCSearch(QdCaCSearchFormModel form) {
//		Pageable pageable = new Pageable();
//
//		Flux<QdCaCPresenter> qdcacResp = this.qdcacRepository.findAll()
//				.map(this.presenterMapper::toPresenter);
//		Mono<PageableResponseModel<QdCaCPresenter>> response = qdcacResp.collectList()
//				.map(items -> 
//					new PageableResponseModel<QdCaCPresenter>(Long.valueOf(items.size()), 1, 10, items)
//				);
//		
//		return response;
		
//		Pageable pageable = new Pageable();
		
		QdCaC qdcac = QdCaC.builder()
			.codi(form.getCodi())
			.jerarquic01(form.getJerarquic01())
			.jerarquic02(form.getJerarquic02())
			.jerarquic03(form.getJerarquic03())
			.serie(form.getSerie())
			.nivell(form.getNivell())
			.nom(form.getNom())
			.potestat(form.getPotestat())
			.promotor(form.getPromotor())
			.build();
		// qdcac.setEstat(1 and 0); 	// TODO: MOSTRAR BAIXES SÍ O NO
		
		ExampleMatcher matcher = ExampleMatcher.matching()
				.withIgnoreNullValues();
		
//		if(form.getMostrarbaixes() == 1) {
//			matcher.withMatcher("dataBaixa", ExampleMatcher.GenericPropertyMatchers.contains());
//		} else if (form.getMostrarbaixes() == 0) {
//			matcher.withIgnorePaths("dataBaixa").withIgnoreNullValues();
//		}			
		
		Example<QdCaC> example = Example.of(qdcac, matcher);
		
		Flux<QdCaCPresenter> qdcacs = this.qdcacRepository.findAll(example)
				.map(this.presenterMapper::toPresenter);
		
		Mono<PageableResponseModel<QdCaCPresenter>> response = qdcacs.collectList()
				.map(items -> 
					new PageableResponseModel<QdCaCPresenter>(Long.valueOf(items.size()), 1, 10, items)
				);
		
		return response;
		
	}


	public Mono<QdCaCPresenter> store(QdCaC qdcac) {
		return this.qdcacRepository.save(qdcac)
			.map(this.presenterMapper::toPresenter);
		/*if (customerNew != null) {
			return customerRepository.save(customerNew);
		}
		return new Customer();*/
	}
	
	public Mono<QdCaCPresenter> getQdCaCById(Long id) {
		return this.qdcacRepository.findById(id)
			.map(this.presenterMapper::toPresenter);
	}
	
	public Mono<QdCaCPresenter> getQdCaCbyCodi(String codi) {
		return this.qdcacRepository.findByCodi(codi)
			.map(this.presenterMapper::toPresenter);
	}
	
	public Mono<QdCaCPresenter> baixaQdCaC(Long id, LocalDate dataBaixa) {
		return this.qdcacRepository.findById(id)
				.map(qdcac -> qdcac.dataBaixa(dataBaixa))
				.flatMap(this.qdcacRepository::save)
				.map(this.presenterMapper::toPresenter);
	}
	
	public Mono<QdCaCPresenter> updateQdCaC(QdCaC qdcacFiltres) {
		final Mono<QdCaCPresenter> qdcac = getQdCaCById(qdcacFiltres.id());
		if (Objects.isNull(qdcac)) {
			   return Mono.empty();
		}
		return this.qdcacRepository.save(qdcacFiltres).map(this.presenterMapper::toPresenter);
		/*
		 * Long num = customerUpdated.getId();
		if (customerRepository.findById(num).isPresent()) {
			Customer customerToUpdate = new Customer();
			customerToUpdate.setId(customerUpdated.getId());
			customerToUpdate.setName(customerUpdated.getName());
			customerToUpdate.setSurname(customerUpdated.getSurname());
			customerToUpdate.setBirtdate(customerUpdated.getBirtdate());
			customerToUpdate.setPhone(customerUpdated.getPhone());
			customerToUpdate.setCountry(customerUpdated.getCountry());
			customerToUpdate.setCity(customerUpdated.getCity());
			customerToUpdate.setDirection(customerUpdated.getDirection());
			customerToUpdate.setPostCode(customerUpdated.getPostCode());
			customerRepository.save(customerToUpdate);
			return "Customer modificado";
		}
		return "Error al modificar el Customer";
		 */
	}
	
//	public Mono<QdCaCPresenter> delete(final Long id) {
//		  final Mono<QdCaCPresenter> dbStudent = getQdCaCById(id);
//		  if (Objects.isNull(dbStudent)) {
//		   return Mono.empty();
//		  }
//		  return getQdCaCById(id)
//				  .switchIfEmpty(Mono.empty())
//				  .filter(Objects::nonNull)
//				  .flatMap(qdcacToBeDeleted -> this.qdcacRepository.delete(qdcacToBeDeleted).then(Mono.just(qdcacToBeDeleted)));
//	
//	
//	/*if (customerRepository.findById(id).isPresent()) {
//			customerRepository.deleteById(id);
//			return "Customer eliminado correctamente.";
//		}
//		return "Error! El customer no existe";*/
//	}
//	
//	public Flux<cat.gencat.clt.git.backend.model.presenter.QdCaC> searchQdCaCs(QdCaC qdcacFilters) {
//		return this.qdcacRepository.findQdCaCByFilters(qdcacFilters)
//			.map(this.presenterMapper::toPresenter);
//	}
    
}
