package cat.gencat.clt.git.backend.security;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class JWTProvider {
    
    private final Key secretKey;

    public String generate(String subject) {
        String token = Jwts.builder()
            .setSubject(subject)
            .setExpiration(Date.from(Instant.now().plus(Duration.ofSeconds(6000)))) //TODO: Use tokenProperties.getMaxAgeSeconds instead
            .setIssuedAt(Date.from(Instant.now()))
            .signWith(this.secretKey)
            .compact();

        return token;
    }

}
