package cat.gencat.clt.git.backend.controller.impl;

import org.springframework.web.bind.annotation.RestController;

import cat.gencat.clt.git.backend.controller.AuditoriaController;
import cat.gencat.clt.git.backend.mapper.PersistencyMapper;
import cat.gencat.clt.git.backend.model.presenter.AuditoriaSearchFormModel;
import cat.gencat.clt.git.backend.model.presenter.AuditoriaPresenter;
import cat.gencat.clt.git.backend.model.presenter.PageableResponseModel;
import cat.gencat.clt.git.backend.service.AuditoriaService;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RestController
public class AuditoriaControllerImpl implements AuditoriaController {

	private final PersistencyMapper persistencyMapper;
    private final AuditoriaService auditoriaService;
    
	@Override
	public Flux<AuditoriaPresenter> all() {
		 return this.auditoriaService.getAuditories();
	}

	@Override
	public Mono<PageableResponseModel<AuditoriaPresenter>> getAuditories(AuditoriaSearchFormModel form) {
		return this.auditoriaService.getAuditSearch(form);
	}

}
