package cat.gencat.clt.git.backend.repositories;

import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.QdCF;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface QdCFRepository extends ReactiveCrudRepository<QdCF, Long>, ReactiveQueryByExampleExecutor<QdCF>, QdCFCriteriaRepository {

	Mono<QdCF> findByCodiAndAmbit(final Long codi, final String ambit);
	Flux<QdCF> findByAmbit(final String ambit);
	
}
