package cat.gencat.clt.git.backend.crosscutting;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import lombok.Builder;
import lombok.Getter;

@Getter
public class GitException extends Exception {

    private GitReasonCode code;
    private List<Object> parameters;

    /**
	 * Default constructor.
	 * 
	 * @param code error code
	 * @param message default message
	 */
	public GitException(GitReason reason) {
		this(reason, null, List.of());
		// super(reason.getMessage());
		// this.code = reason.getCode();
        // this.parameters = Collections.unmodifiableList(List.of());
	}

	/**
	 * Constructor from a captured exception.
	 * 
	 * @param code error code
	 * @param message default message
	 * @param cause captured exception, error origin
	 */
	public GitException(GitReason reason, Throwable cause) {
		this(reason, cause, List.of());
	}

	/**
	 * Constructor from a code and a set of parameters to use when building the
	 * corresponding message
	 * 
	 * @param code error code
	 * @param message default message
	 * @param parameters the array of parameters to build the corresponding message
	 */
	public GitException(GitReason reason, Object... parameters) {
		this(reason, null, parameters);
	}

	/**
	 * Constructor from a captured exception and a set of parameters to use when
	 * building the corresponding message
	 * 
	 * @param code error code
	 * @param message default message
	 * @param cause captured exception, error origin
	 * @param parameters the array of parameters to build the corresponding message
	 */
	@Builder
	public GitException(GitReason reason, Throwable cause, Object... parameters) {
		super(reason.getMessage(), cause);		//TODO: Optional reason
		this.code = reason.getCode();			//TODO: Optional reason
        this.parameters = Collections.unmodifiableList(Optional.ofNullable(parameters).map(List::of).orElse(List.of()));
	}

}
