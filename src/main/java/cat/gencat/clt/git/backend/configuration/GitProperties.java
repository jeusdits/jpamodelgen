package cat.gencat.clt.git.backend.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "git")
@Getter
@Setter
public class GitProperties {
    
    private LoginProperties loginService;
    private TokenProperties tokenService;

}
