package cat.gencat.clt.git.backend.repositories;

import java.util.List;

import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.AccioAudit;
import io.smallrye.mutiny.Uni;

@Repository
public interface AccioAuditRepository {

    Uni<List<AccioAudit>> findAll();

}
