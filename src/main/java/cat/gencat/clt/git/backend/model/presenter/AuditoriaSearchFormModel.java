package cat.gencat.clt.git.backend.model.presenter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuditoriaSearchFormModel extends Pageable {

	private Long id;
	private String usuari;
	private String instrument;
	
}
