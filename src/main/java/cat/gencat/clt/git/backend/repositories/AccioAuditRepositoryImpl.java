package cat.gencat.clt.git.backend.repositories;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.AccioAudit;
import io.smallrye.mutiny.Uni;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class AccioAuditRepositoryImpl implements AccioAuditRepository {

    private final Mutiny.SessionFactory sessionFactory;

    @Override
    public Uni<List<AccioAudit>> findAll() {
        CriteriaBuilder criteriaBuilder = this.sessionFactory.getCriteriaBuilder();
        CriteriaQuery<AccioAudit> query = criteriaBuilder.createQuery(AccioAudit.class);

        query.from(AccioAudit.class);

        return this.sessionFactory.withSession(session -> session.createQuery(query).getResultList());
    }

}
