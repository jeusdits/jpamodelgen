package cat.gencat.clt.git.backend.repositories;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.Auditoria;

@Repository
public interface AuditoriaRepository extends ReactiveCrudRepository<Auditoria, Long>{

}
