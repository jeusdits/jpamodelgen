package cat.gencat.clt.git.backend.model.presenter;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Promotor {

	@NotNull
	private Long id;
	
	@NotNull
    private String codi;
	
	@NotNull
    private String nom;
    
    private Date dataInici;
    
    private Date dataFi;
    
}
