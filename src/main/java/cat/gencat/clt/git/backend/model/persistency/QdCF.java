package cat.gencat.clt.git.backend.model.persistency;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Table(name = "GITTBQDCF")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBQDCF")
public class QdCF {
    
	@Id
	@org.springframework.data.annotation.Id
	@Column(name = "QDCF_ID")
	@org.springframework.data.relational.core.mapping.Column("QDCF_ID")
    private Long id;
	
	@Column(name = "QDCF_CODI")
	@org.springframework.data.relational.core.mapping.Column("QDCF_CODI")
    private Long codi;
    
	@Column(name = "QDCF_JERARQUIC01")
	@org.springframework.data.relational.core.mapping.Column("QDCF_JERARQUIC01")
    private String jerarquic01;
	
	@Column(name = "QDCF_JERARQUIC02")
	@org.springframework.data.relational.core.mapping.Column("QDCF_JERARQUIC02")
    private String jerarquic02;
	
	@Column(name = "QDCF_JERARQUIC03")
	@org.springframework.data.relational.core.mapping.Column("QDCF_JERARQUIC03")
    private String jerarquic03;
	
	@Column(name = "QDCF_JERARQUIC04")
	@org.springframework.data.relational.core.mapping.Column("QDCF_JERARQUIC04")
    private String jerarquic04;
	
	@Column(name = "QDCF_JERARQUIC05")
	@org.springframework.data.relational.core.mapping.Column("QDCF_JERARQUIC05")
    private String jerarquic05;

	@Column(name = "QDCF_NIVELL")
	@org.springframework.data.relational.core.mapping.Column("QDCF_NIVELL")
    private Long nivell;
	
	@Column(name = "QDCF_AMBIT")
	@org.springframework.data.relational.core.mapping.Column("QDCF_AMBIT")
    private String ambit;
	
	@Column(name = "QDCF_NOM")
	@org.springframework.data.relational.core.mapping.Column("QDCF_NOM")
    private String nom;
	
	@Column(name = "QDCF_DESCRIPCIO")
	@org.springframework.data.relational.core.mapping.Column("QDCF_DESCRIPCIO")
    private String descripcio;
	
	@Column(name = "QDCF_SERIE")
	@org.springframework.data.relational.core.mapping.Column("QDCF_SERIE")
    private Long serie;
	
	@Column(name = "QDCF_POTESTAT")
	@org.springframework.data.relational.core.mapping.Column("QDCF_POTESTAT")
    private Long potestat;
	
	@Column(name = "QDCF_PROMOTOR")
	@org.springframework.data.relational.core.mapping.Column("QDCF_PROMOTOR")
    private Long promotor;
	
	@Column(name = "QDCF_FUNCIOADM")
	@org.springframework.data.relational.core.mapping.Column("QDCF_FUNCIOADM")
    private String funcioadm;
	
	@Column(name = "QDCF_SERIESREL")
	@org.springframework.data.relational.core.mapping.Column("QDCF_SERIESREL")
    private String seriesrel;
	
	@Column(name = "QDCF_DOCUMENTS")
	@org.springframework.data.relational.core.mapping.Column("QDCF_DOCUMENTS")
    private String documents;
	
	@Column(name = "QDCF_MARCLEGAL")
	@org.springframework.data.relational.core.mapping.Column("QDCF_MARCLEGAL")
    private String marclegal;
	
	@Column(name = "QDCF_TAAD")
	@org.springframework.data.relational.core.mapping.Column("QDCF_TAAD")
    private Long taad;
	
	@Column(name = "QDCF_CODICCP")
	@org.springframework.data.relational.core.mapping.Column("QDCF_CODICCP")
    private String codiccp;
	
	@Column(name = "QDCF_NOMPROCES")
	@org.springframework.data.relational.core.mapping.Column("QDCF_NOMPROCES")
    private String nomproces;
	
	@Column(name = "QDCF_SIA")
	@org.springframework.data.relational.core.mapping.Column("QDCF_SIA")
    private Long sia;
	
	@Column(name = "QDCF_DATAALTA")
	@org.springframework.data.relational.core.mapping.Column("QDCF_DATAALTA")
    private LocalDate dataAlta;
	
	@Column(name = "QDCF_DATABAIXA")
	@org.springframework.data.relational.core.mapping.Column("QDCF_DATABAIXA")
    private LocalDate dataBaixa;
	
	@Column(name = "QDCF_OBSERVACIONS")
	@org.springframework.data.relational.core.mapping.Column("QDCF_OBSERVACIONS")
    private String observacions;
	
	@Column(name = "QDCF_ESTAT")
	@org.springframework.data.relational.core.mapping.Column("QDCF_ESTAT")
    private Long estat;
	
	@Column(name = "QDCF_PRITRAPUBLICACIO")
	@org.springframework.data.relational.core.mapping.Column("QDCF_PRITRAPUBLICACIO")
    private LocalDate pritraPublicacio;
	
	@Column(name = "QDCF_DARTRAPUBLICACIO")
	@org.springframework.data.relational.core.mapping.Column("QDCF_DARTRAPUBLICACIO")
    private LocalDate dartraPublicacio;
	
	@Column(name = "QDCF_PAREID")
	@org.springframework.data.relational.core.mapping.Column("QDCF_PAREID")
    private Long pareid;
	
}
