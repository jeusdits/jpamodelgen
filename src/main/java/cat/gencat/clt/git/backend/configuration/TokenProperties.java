package cat.gencat.clt.git.backend.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "jwt")
@Getter
@Setter
public class TokenProperties {
    
    private long maxAgeSeconds;
    private String secret;

}
