package cat.gencat.clt.git.backend.crosscutting;

public interface GitPolicy {
    
    String getCode();

    String getMessage();

}
