package cat.gencat.clt.git.backend.model.presenter;

import java.util.List;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class PageableResponseModel<T> {
	private static final Integer DEFAULT_PAGE_SIZE		= 10;


	private Long totalResults;
	private Integer page;
	private Integer size;
	private List<T> results;
	
	public Integer getSize() {
		return
			Objects.nonNull(size)
				? size
				: DEFAULT_PAGE_SIZE;
	}

	public void setSize(Integer size) {
		this.size =
			Objects.nonNull(size)
				? size
				: DEFAULT_PAGE_SIZE;
	}
	
}
