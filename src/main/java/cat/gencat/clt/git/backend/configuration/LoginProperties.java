package cat.gencat.clt.git.backend.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(prefix = "login")
@Getter
@Setter
public class LoginProperties {
    
    private String urlUi;
    private Integer portUi;
    private String pathUi;

}
