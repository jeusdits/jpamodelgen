package cat.gencat.clt.git.backend.model.persistency;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "GITTBTAAD")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBTAAD")
public class Taad {

	@Id
    @org.springframework.data.annotation.Id
	@Column(name = "TAAD_ID")
    @org.springframework.data.relational.core.mapping.Column("TAAD_ID")
	private Long id;
	
	@Column(name = "TAAD_CODI")
    @org.springframework.data.relational.core.mapping.Column("TAAD_CODI")
    private String codi;
	
    @Column(name = "TAAD_SERIE")
    @org.springframework.data.relational.core.mapping.Column("TAAD_SERIE")
    private String serie;
    
    @Column(name = "TAAD_AMBIT1")
    @org.springframework.data.relational.core.mapping.Column("TAAD_AMBIT1")
    private int ambit1;
    
    @Column(name = "TAAD_AMBIT2")
    @org.springframework.data.relational.core.mapping.Column("TAAD_AMBIT2")
    private int ambit2;
    
    @Column(name = "TAAD_FUNCADM")
    @org.springframework.data.relational.core.mapping.Column("TAAD_FUNCADM")
    private String funcAdm;
    
    @Column(name = "TAAD_MARCLEGAL")
    @org.springframework.data.relational.core.mapping.Column("TAAD_MARCLEGAL")
    private String marcLegal;
    
    @Column(name = "TAAD_DOCUMENTS")
    @org.springframework.data.relational.core.mapping.Column("TAAD_DOCUMENTS")
    private String documents;
    
    @Column(name = "TAAD_DOCUMENTSRECAP")
    @org.springframework.data.relational.core.mapping.Column("TAAD_DOCUMENTSRECAP")
    private String documentsRecap;
    
    @Column(name = "TAAD_DISPOSICIO")
    @org.springframework.data.relational.core.mapping.Column("TAAD_DISPOSICIO")
    private int disposicio;
    
    @Column(name = "TAAD_TERMINIDISPOSICIO1")
    @org.springframework.data.relational.core.mapping.Column("TAAD_TERMINIDISPOSICIO1")
    private int terminiDisposicio1;
    
    @Column(name = "TAAD_TERMINIAPLICACIO")
    @org.springframework.data.relational.core.mapping.Column("TAAD_TERMINIAPLICACIO")
    private String terminiAplicacio;
    
    @Column(name = "TAAD_TERMINIDISPOSICIO2")
    @org.springframework.data.relational.core.mapping.Column("TAAD_TERMINIDISPOSICIO2")
    private int terminiDisposicio2;
    
    @Column(name = "TAAD_TERMINIVIGENCIA")
    @org.springframework.data.relational.core.mapping.Column("TAAD_TERMINIVIGENCIA")
    private int terminiVigencia;
    
    @Column(name = "TAAD_VALORADM")
    @org.springframework.data.relational.core.mapping.Column("TAAD_VALORADM")
    private int valorAdm;
    
    @Column(name = "TAAD_VALORLEGAL")
    @org.springframework.data.relational.core.mapping.Column("TAAD_VALORLEGAL")
    private int valorLegal;
    
    @Column(name = "TAAD_INFORMATIU")
    @org.springframework.data.relational.core.mapping.Column("TAAD_INFORMATIU")
    private int informatiu;
    
    @Column(name = "TAAD_REGIM")
    @org.springframework.data.relational.core.mapping.Column("TAAD_REGIM")
    private int regim;
    
    @Column(name = "TAAD_MOTIVACIO")
    @org.springframework.data.relational.core.mapping.Column("TAAD_MOTIVACIO")
    private int motivacio;
    
    @Column(name = "TAAD_VIGENCIA")
    @org.springframework.data.relational.core.mapping.Column("TAAD_VIGENCIA")
    private int vigencia;
    
    @Column(name = "TAAD_DATAINICI")
    @org.springframework.data.relational.core.mapping.Column("TAAD_DATAINICI")
    private LocalDate dataInici;
    
    @Column(name = "TAAD_DATAFI")
    @org.springframework.data.relational.core.mapping.Column("TAAD_DATAFI")
    private LocalDate dataFi;
    
    @Column(name = "TAAD_FOMANJURIDICA")
    @org.springframework.data.relational.core.mapping.Column("TAAD_FOMANJURIDICA")
    private String fomanJuridica;
    
    @Column(name = "TAAD_ORGANISME")
    @org.springframework.data.relational.core.mapping.Column("TAAD_ORGANISME")
    private int organisme;
    
    @Column(name = "TAAD_ORDRE")
    @org.springframework.data.relational.core.mapping.Column("TAAD_ORDRE")
    private String ordre;
    
    @Column(name = "TAAD_DOGC")
    @org.springframework.data.relational.core.mapping.Column("TAAD_DOGC")
    private String dogc;
    
    @Column(name = "TAAD_SITUACIO")
    @org.springframework.data.relational.core.mapping.Column("TAAD_SITUACIO")
    private int situacio;
    
    @Column(name = "TAAD_SERIESREL")
    @org.springframework.data.relational.core.mapping.Column("TAAD_SERIESREL")
    private String seriesRel;
    
    @Column(name = "TAAD_SERIESANT")
    @org.springframework.data.relational.core.mapping.Column("TAAD_SERIESANT")
    private String seriesAnt;
    
    @Column(name = "TAAD_OBSERVACIONS")
    @org.springframework.data.relational.core.mapping.Column("TAAD_OBSERVACIONS")
    private String observacions;
    
    @Column(name = "TAAD_ALTREFONJUR")
    @org.springframework.data.relational.core.mapping.Column("TAAD_ALTREFONJUR")
    private String altreFonJur;
    
    @Column(name = "TAAD_PRITRAPUBLICACIO")
    @org.springframework.data.relational.core.mapping.Column("TAAD_PRITRAPUBLICACIO")
    private LocalDate pritraPublicacio;
    
    @Column(name = "TAAD_DARTRAPUBLICACIO")
    @org.springframework.data.relational.core.mapping.Column("TAAD_DARTRAPUBLICACIO")
    private LocalDate dartraPublicacio;
    

}
