package cat.gencat.clt.git.backend.crosscutting;

import lombok.Getter;

@Getter
public enum GitResource {

    SERVICE             ("service"),
    QCDF                ("service.qcdf");

    private String code;

    private GitResource(String code) {
        this.code = code;
    }

}

