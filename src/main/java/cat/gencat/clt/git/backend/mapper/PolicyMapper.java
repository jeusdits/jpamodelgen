package cat.gencat.clt.git.backend.mapper;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.support.WebExchangeBindException;

import cat.gencat.clt.git.backend.controller.ApiError;
import cat.gencat.clt.git.backend.controller.ApiValidationError;
import cat.gencat.clt.git.backend.crosscutting.GitException;
import cat.gencat.clt.git.backend.crosscutting.GitReason;
import cat.gencat.clt.git.backend.crosscutting.GitReasonCode;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class PolicyMapper {
    
    private static final String ERR_POSTFIX = "ERROR";
    private static final String PREFIX_SEPARATOR = ".";

    private final MessageSource messageSource;

    /**
     * Maps a 
     */
    public ApiError toApiError(GitException policy) {
        return ApiError.builder()
            .code(this.toString(policy.getCode()))
            .description(
                this.messageSource.getMessage(
                    String.join(PREFIX_SEPARATOR, ERR_POSTFIX, this.toString(policy.getCode())),
                    policy.getParameters().toArray(new String[0]),
                    String.format(policy.getMessage(), policy.getParameters()),
                    Locale.getDefault()
                )
            )
            .build();
    }

    /**
     * Maps a
     */
    public ApiError toApiError(WebExchangeBindException policy) {
        List<ApiValidationError> validationErrors = policy.getBindingResult()
            .getFieldErrors()
            .stream()
            .map(this::toValidationError)
            .collect(Collectors.toList());

        return ApiError.builder()
            .code(this.toString(GitReason.VALIDATION_FAILED.getCode()))
            .description(
                this.messageSource.getMessage(
                    String.join(PREFIX_SEPARATOR, this.toString(GitReason.VALIDATION_FAILED.getCode()), ERR_POSTFIX),
                    null,
                    GitReason.VALIDATION_FAILED.getMessage(),
                    Locale.getDefault()
                )
            )
            .errors(validationErrors)
            .build();
    }

    /**
     * Maps a
     */
    public ApiError toApiError(Exception policy) {
        return ApiError.builder()
            .code("code")
            .description("description")
            .build();
    }

    private ApiValidationError toValidationError(FieldError fieldError) {
        return ApiValidationError.builder()
            .field(fieldError.getField())
            .message(fieldError.getDefaultMessage())
            .rejectedValue(fieldError.getRejectedValue())
            .object(fieldError.getObjectName())
            .build();
    }

    private String toString(GitReasonCode gitReasonCode) {
        return gitReasonCode.name();
    }
}
