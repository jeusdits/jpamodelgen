package cat.gencat.clt.git.backend.model.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Pageable {
	
	private String sortField;
	private int sortOrder;
	private int page;
	private int size;
	
}
