package cat.gencat.clt.git.backend.repositories;

import java.util.List;
import java.util.function.BiFunction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.QdCF;
import io.smallrye.mutiny.Uni;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class QdCFCriteriaRepositoryImpl implements QdCFCriteriaRepository {

    private final Mutiny.SessionFactory sessionFactory;

    @Override
    public Uni<Page<QdCF>> findByDataBaixaNull(Pageable pageable) {
        CriteriaBuilder criteriaBuilder = this.sessionFactory.getCriteriaBuilder();

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        countQuery.select(criteriaBuilder.count(countQuery.from(QdCF.class)));

        CriteriaQuery<QdCF> criteriaQuery = criteriaBuilder.createQuery(QdCF.class);
        Root<QdCF> qdcf = criteriaQuery.from(QdCF.class);
        Predicate predicate = criteriaBuilder.isNull(qdcf.get("dataBaixa"));
        criteriaQuery.where(predicate);

        Uni<Long> count = this.sessionFactory.withSession(session -> session.createQuery(countQuery).getSingleResult());
        Uni<List<QdCF>> items = this.sessionFactory.withSession(session -> session.createQuery(criteriaQuery).getResultList());
        BiFunction<Long, List<QdCF>, Page<QdCF>> buildPage = (total, qdcfs) -> new PageImpl<>(qdcfs, pageable, total);
        return Uni.combine().all().unis(count, items)
            .combinedWith(buildPage);
    }

    @Override
    public Uni<Long> getLastCode(String ambit) {
        CriteriaBuilder criteriaBuilder = this.sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);

        Root<QdCF> qdcf = query.from(QdCF.class);

        Predicate filter = criteriaBuilder.equal(qdcf.get("ambit"), ambit);
        Selection<Long> maxCodeSelection = criteriaBuilder.max(qdcf.get("codi"));

        query.select(maxCodeSelection);
        // Optional<T>.ofNullable(ambit)
        //     .map(criteriaBuilder.equal(qdcf.get("ambit"), ambit))
        //     .orElse(criteriaBuilder.)
            // .where(filter);

        return this.sessionFactory.withSession(session -> session.createQuery(query).getSingleResult());
    }
    
}
