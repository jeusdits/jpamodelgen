package cat.gencat.clt.git.backend.model.persistency;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "GITTBAUDI")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBAUDI")
public class Auditoria {

	@Id
    @org.springframework.data.annotation.Id
	@NotNull
	@Column(name = "AUDI_ID")
    @org.springframework.data.relational.core.mapping.Column("AUDI_ID")
	private Long id;
	
	@NotNull
	@Column(name = "AUDI_USUARI")
    @org.springframework.data.relational.core.mapping.Column("AUDI_USUARI")
    private String usuari;
	
	@NotNull
    @Column(name = "AUDI_DATA")
    @org.springframework.data.relational.core.mapping.Column("AUDI_DATA")
    private LocalDate data;
    
	@NotNull
    @Column(name = "AUDI_ACCIO")
    @org.springframework.data.relational.core.mapping.Column("AUDI_ACCIO")
    private String accio;
    
	@NotNull
    @Column(name = "AUDI_INSTRUMENT")
    @org.springframework.data.relational.core.mapping.Column("AUDI_INSTRUMENT")
    private int instrument;
    
	@NotNull
    @Column(name = "AUDI_IDELEMENT")
    @org.springframework.data.relational.core.mapping.Column("AUDI_IDELEMENT")
    private String idElement;
    
    @Column(name = "AUDI_OBSERVACIONS")
    @org.springframework.data.relational.core.mapping.Column("AUDI_OBSERVACIONS")
    private String observacions;
    
}
