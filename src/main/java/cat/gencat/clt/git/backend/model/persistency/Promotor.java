package cat.gencat.clt.git.backend.model.persistency;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "GITTBPROMOTOR")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBPROMOTOR")
public class Promotor {

	@Id
    @org.springframework.data.annotation.Id
	@Column(name = "PROMOTOR_ID")
    @org.springframework.data.relational.core.mapping.Column("PROMOTOR_ID")
	private Long id;
	
	@Column(name = "PROMOTOR_CODI")
    @org.springframework.data.relational.core.mapping.Column("PROMOTOR_CODI")
    private String codi;
	
    @Column(name = "PROMOTOR_DENOMINACIO")
    @org.springframework.data.relational.core.mapping.Column("PROMOTOR_DENOMINACIO")
    private String nom;
    
    @Column(name = "PROMOTOR_DATAINICI")
    @org.springframework.data.relational.core.mapping.Column("PROMOTOR_DATAINICI")
    private Date dataInici;
    
    @Column(name = "PROMOTOR_DATAFI")
    @org.springframework.data.relational.core.mapping.Column("PROMOTOR_DATAFI")
    private Date dataFi;
    
}
