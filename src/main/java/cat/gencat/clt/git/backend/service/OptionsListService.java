package cat.gencat.clt.git.backend.service;

import java.util.List;

import org.springframework.stereotype.Service;

import cat.gencat.clt.git.backend.mapper.PresenterMapper;
import cat.gencat.clt.git.backend.model.presenter.AccioAuditPresenter;
import cat.gencat.clt.git.backend.model.presenter.Ambit;
import cat.gencat.clt.git.backend.model.presenter.CodiSIA;
import cat.gencat.clt.git.backend.model.presenter.Instrument;
import cat.gencat.clt.git.backend.model.presenter.PotestatAdmin;
import cat.gencat.clt.git.backend.model.presenter.Promotor;
import cat.gencat.clt.git.backend.model.presenter.TaadPresenter;
import cat.gencat.clt.git.backend.repositories.AccioAuditRepository;
import cat.gencat.clt.git.backend.repositories.AmbitRepository;
import cat.gencat.clt.git.backend.repositories.CodiSIARepository;
import cat.gencat.clt.git.backend.repositories.InstrumentRepository;
import cat.gencat.clt.git.backend.repositories.PotestatAdminRepository;
import cat.gencat.clt.git.backend.repositories.PromotorRepository;
import cat.gencat.clt.git.backend.repositories.TaadRepository;
import io.smallrye.mutiny.Uni;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@RequiredArgsConstructor
@Service
public class OptionsListService {
	
	private final PresenterMapper presenterMapper;
	
	private final AmbitRepository ambitRepository;
	private final InstrumentRepository instrumentRepository;
	private final AccioAuditRepository accioAuditRepository;
	private final PotestatAdminRepository potestatAdminRepository;
	private final PromotorRepository promotorRepository;
	private final CodiSIARepository codiSIARepository;
	private final TaadRepository taadRepository;
	
	public Flux<Ambit> getAmbits() {
		return ambitRepository.findAll().map(this.presenterMapper::toPresenter);
	}	
	
	public Flux<Instrument> getInstruments() {
		return instrumentRepository.findAll().map(this.presenterMapper::toPresenter);
	}	
	
	public Uni<List<AccioAuditPresenter>> getAccionsAudits() {
		return accioAuditRepository.findAll().map(this.presenterMapper::toPresenter);
	}	
	
	public Flux<PotestatAdmin> getPotestatAdmin() {
		return potestatAdminRepository.findAll().map(this.presenterMapper::toPresenter);
	}
	
	public Flux<Promotor> getPromotors() {
		return promotorRepository.findAll().map(this.presenterMapper::toPresenter);
	}
	
	public Flux<CodiSIA> getCodiSIA() {
		return codiSIARepository.findAll().map(this.presenterMapper::toPresenter);
	}
	
	public Flux<TaadPresenter> getTAADs() {
		return taadRepository.findAll().map(this.presenterMapper::toPresenter);
	}

}
