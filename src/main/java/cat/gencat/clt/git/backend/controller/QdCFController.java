package cat.gencat.clt.git.backend.controller;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cat.gencat.clt.git.backend.crosscutting.GitException;
import cat.gencat.clt.git.backend.crosscutting.GitReason;
import cat.gencat.clt.git.backend.mapper.PersistencyMapper;
import cat.gencat.clt.git.backend.model.presenter.PageableResponseModel;
import cat.gencat.clt.git.backend.model.presenter.QdCFPresenter;
import cat.gencat.clt.git.backend.model.presenter.QdCFSearchFormModel;
import cat.gencat.clt.git.backend.service.QdCFService;
import io.smallrye.mutiny.Uni;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/qdcf")
@RequiredArgsConstructor
public class QdCFController {

    private final PersistencyMapper persistencyMapper;
    private final QdCFService qdcfService;

    @GetMapping
    public Uni<Page<QdCFPresenter>> all() {
        return this.qdcfService.getQdCFs(Pageable.unpaged());
    }
    
    @GetMapping ("/search")
    public Mono<PageableResponseModel<QdCFPresenter>> getQdCFSearch(@Valid QdCFSearchFormModel form, Pageable pageable) {
        return this.qdcfService.getQdCFSearch(form);
    }

    @PostMapping ("/alta")
    public Mono<QdCFPresenter> save(@RequestBody @Valid QdCFPresenter qdcf) {
    	return this.qdcfService.store(
            this.persistencyMapper.toPersistency(qdcf)
        );
    }

    @PostMapping(path = "exception")
    public Mono<QdCFPresenter> exception() {
        return Mono.error(
            GitException.builder()
                .reason(GitReason.QCDF_NOT_UPDATABLE)
                .parameters(new String[]{"any_id"})
                .build()
            );
    }
    
    @PutMapping ("/baixa")
    public Mono<QdCFPresenter> baixaQdCF(@RequestBody QdCFPresenter qdcf) {
    	return this.qdcfService.baixaQdCF(qdcf.getId(), qdcf.getDataBaixa());
    } 
    
    @GetMapping("/{id}")
    public Mono<QdCFPresenter> getQdCFById(@PathVariable Long id) {
        return this.qdcfService.getQdCFById(id);
    }
    
    @GetMapping("/search/codi")
    public Mono<QdCFPresenter> getQdCFbyCodi(@RequestParam(value = "codi") String codi) {
    	return this.qdcfService.getQdCFbyCodi(codi);
    }
    
    @GetMapping("/lastCodi")
    public Uni<Long> getQdCFlast(String ambit) {
    	return this.qdcfService.getQdCFlastCodi(ambit);
    }
    
    @PutMapping("/edicio")
    public Mono<QdCFPresenter> update(@RequestBody QdCFPresenter qdcf) {
        return this.qdcfService.updateQdCF(
        		this.persistencyMapper.toPersistency(qdcf)
        );
    }
    
//    @GetMapping("/search")
//    public Flux<QdCF> search(@RequestBody QdCF qdcfFilters) {
//        return this.qdcfService.searchQdCFs(qdcfFilters);
//    }
}
