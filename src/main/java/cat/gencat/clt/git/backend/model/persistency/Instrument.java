package cat.gencat.clt.git.backend.model.persistency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "GITTBINSTRUMENTS")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBINSTRUMENTS")
public class Instrument {

	@Id
	@org.springframework.data.annotation.Id
	@Column(name = "INSTRUMENTS_ID")
	@org.springframework.data.relational.core.mapping.Column("INSTRUMENTS_ID")
	private Long id;
	
	@Column(name = "INSTRUMENTS_CODI")
	@org.springframework.data.relational.core.mapping.Column("INSTRUMENTS_CODI")
    private String codi;
	
    @Column(name = "INSTRUMENTS_NOM")
	@org.springframework.data.relational.core.mapping.Column("INSTRUMENTS_NOM")
    private String nom;
    
}
