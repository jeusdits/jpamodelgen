package cat.gencat.clt.git.backend.controller;

import java.net.URI;
import java.security.Principal;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import cat.gencat.clt.git.backend.security.JWTProvider;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@RestController
public class GicarController {

    static final String COOKIE_ACCESS_TOKEN = "access_token";
    static final String COOKIE_REFRESH_TOKEN = "refresh_token";
    static final String GICAR_ID_HEADER = "GICAR_ID";

    private final JWTProvider jwtProvider;
    private final URI redirectionURI;

    /**
    * Tries to extract header looking up {@link GicarController.GICAR_ID_HEADER} from {@code request}.
    * @param request
    * @return Value of {@code gicarHeader}. Otherwise, {@code ""}
    */
    @PostMapping("/gicar")
    public Mono<ResponseEntity<Void>> gicar(@RequestHeader(GICAR_ID_HEADER) String gicarId) {
        ResponseCookie accessTokenCookie = this.buildCookie(
            COOKIE_ACCESS_TOKEN,
            this.jwtProvider.generate(gicarId)
        );

        return Mono.just(
            ResponseEntity
                .status(HttpStatus.TEMPORARY_REDIRECT)
                .header(HttpHeaders.LOCATION, redirectionURI.toString())
                .header(HttpHeaders.SET_COOKIE, accessTokenCookie.toString())
                .build()
        );
    }

    @GetMapping("/me")
    public Mono<String> me(Mono<Principal> principal) {
        return principal
          .map(Principal::getName)
          .map(name -> String.format("Hello, %s", name));
    }

    /**
     * Build cookie with name to carry content 
     */
    private ResponseCookie buildCookie(String cookieName, String cookieContent) {
        ResponseCookie accessTokenCookie = ResponseCookie.from(
            cookieName,
            cookieContent
        )
        .httpOnly(true)
        .secure(true)
        .path("/")
        .maxAge(60)
        .build();

        return accessTokenCookie;
    }

}
