package cat.gencat.clt.git.backend.controller.impl;

import java.util.List;

import org.springframework.web.bind.annotation.RestController;

import cat.gencat.clt.git.backend.controller.OptionsListController;
import cat.gencat.clt.git.backend.model.presenter.AccioAuditPresenter;
import cat.gencat.clt.git.backend.model.presenter.Ambit;
import cat.gencat.clt.git.backend.model.presenter.CodiSIA;
import cat.gencat.clt.git.backend.model.presenter.Instrument;
import cat.gencat.clt.git.backend.model.presenter.PotestatAdmin;
import cat.gencat.clt.git.backend.model.presenter.Promotor;
import cat.gencat.clt.git.backend.model.presenter.TaadPresenter;
import cat.gencat.clt.git.backend.service.OptionsListService;
import io.smallrye.mutiny.Uni;
import lombok.AllArgsConstructor;
import reactor.core.publisher.Flux;

@AllArgsConstructor
@RestController
public class OptionsListControllerImpl implements OptionsListController {
	
	private OptionsListService optionsListService;



	@Override
	public Flux<Ambit> getAmbitsList() {
		return optionsListService.getAmbits();
	}


	@Override
	public Flux<Instrument> getInstrumentsList() {
		return optionsListService.getInstruments();
	}


	@Override
	public Uni<List<AccioAuditPresenter>> getAccionsAuditsList() {
		return optionsListService.getAccionsAudits();
	}


	@Override
	public Flux<PotestatAdmin> getPotestatAdminList() {
		return optionsListService.getPotestatAdmin();
	}


	@Override
	public Flux<Promotor> getPromotorsList() {
		return optionsListService.getPromotors();
	}


	@Override
	public Flux<CodiSIA> getCodiSIAList() {
		return optionsListService.getCodiSIA();
	}


	@Override
	public Flux<TaadPresenter> getTAADsList() {
		return optionsListService.getTAADs();
	}


	

}
