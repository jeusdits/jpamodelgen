package cat.gencat.clt.git.backend.model.persistency;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "GITTBSIA")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBSIA")
public class CodiSIA {

	@Id
    @org.springframework.data.annotation.Id
	@Column(name = "SIA_ID")
    @org.springframework.data.relational.core.mapping.Column("SIA_ID")
	private Long id;
	
	@Column(name = "SIA_CODI")
    @org.springframework.data.relational.core.mapping.Column("SIA_CODI")
    private String codi;
	
    @Column(name = "SIA_DENOMINACIO")
    @org.springframework.data.relational.core.mapping.Column("SIA_DENOMINACIO")
    private String nom;
    
    @Column(name = "SIA_DATAINICI")
    @org.springframework.data.relational.core.mapping.Column("SIA_DATAINICI")
    private LocalDate dataInici;
    
    @Column(name = "SIA_DATAFI")
    @org.springframework.data.relational.core.mapping.Column("SIA_DATAFI")
    private LocalDate dataFi;
    
}
