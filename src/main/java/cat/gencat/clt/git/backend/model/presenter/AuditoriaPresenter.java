package cat.gencat.clt.git.backend.model.presenter;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class AuditoriaPresenter {
	
	@NotNull
	private Long id;
	
	@NotNull
	private String usuari;
	
	@NotNull
    private LocalDate data;
    
	@NotNull
    private String accio;
    
	@NotNull
    private int instrument;
    
	@NotNull
    private String idElement;
    
    private String observacions;

}
