package cat.gencat.clt.git.backend.model.persistency;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Accessors(fluent = true)
@Table(name = "GITTBQDCAC")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBQDCAC")
public class QdCaC {
    
	@Id
	@org.springframework.data.annotation.Id
	@Column(name = "QDCAC_ID")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_ID")
    private Long id;
	
	@Column(name = "QDCAC_CODI")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_CODI")
    private String codi;
    
	@Column(name = "QDCAC_JERARQUIC01")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_JERARQUIC01")
    private String jerarquic01;
	
	@Column(name = "QDCAC_JERARQUIC02")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_JERARQUIC02")
    private String jerarquic02;
	
	@Column(name = "QDCAC_JERARQUIC03")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_JERARQUIC03")
    private String jerarquic03;
	
	@Column(name = "QDCAC_NIVELL")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_NIVELL")
    private Long nivell;
	
	@Column(name = "QDCAC_NOM")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_NOM")
    private String nom;
	
	@Column(name = "QDCAC_TITOL")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_TITOL")
    private String titol;
	
	@Column(name = "QDCAC_ALTRESNOMS")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_ALTRESNOMS")
    private String altresNoms;
	
	@Column(name = "QDCAC_REPRESENTA")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_REPRESENTA")
    private Long representa;
		
	@Column(name = "QDCAC_DESCRIPCIO")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_DESCRIPCIO")
    private String descripcio;
	
	@Column(name = "QDCAC_SERIE")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_SERIE")
    private Long serie;
	
	@Column(name = "QDCAC_POTESTAT")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_POTESTAT")
    private Long potestat;
	
	@Column(name = "QDCAC_PROMOTOR")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_PROMOTOR")
    private Long promotor;
	
	@Column(name = "QDCAC_FUNCIOADM")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_FUNCIOADM")
    private String funcioadm;
	
	@Column(name = "QDCAC_SERIESREL")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_SERIESREL")
    private String seriesrel;
	
	@Column(name = "QDCAC_DOCUMENTS")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_DOCUMENTS")
    private String documents;
	
	@Column(name = "QDCAC_MARCLEGAL")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_MARCLEGAL")
    private String marclegal;
	
	@Column(name = "QDCAC_TAAD")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_TAAD")
    private Long taad;
		
	@Column(name = "QDCAC_SIA")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_SIA")
    private Long sia;
	
	@Column(name = "QDCAC_DATAALTA")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_DATAALTA")
    private LocalDate dataAlta;
	
	@Column(name = "QDCAC_DATABAIXA")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_DATABAIXA")
    private LocalDate dataBaixa;
	
	@Column(name = "QDCAC_OBSERVACIONS")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_OBSERVACIONS")
    private String observacions;
	
	@Column(name = "QDCAC_ESTAT")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_ESTAT")
    private Long estat;
	
	@Column(name = "QDCAC_PRITRAPUBLICACIO")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_PRITRAPUBLICACIO")
    private LocalDate pritraPublicacio;
	
	@Column(name = "QDCAC_DARTRAPUBLICACIO")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_DARTRAPUBLICACIO")
    private LocalDate dartraPublicacio;
	
	@Column(name = "QDCAC_PAREID")
	@org.springframework.data.relational.core.mapping.Column("QDCAC_PAREID")
    private Long pareid;
	
}
