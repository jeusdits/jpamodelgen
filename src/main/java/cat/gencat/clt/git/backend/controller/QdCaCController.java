package cat.gencat.clt.git.backend.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cat.gencat.clt.git.backend.crosscutting.GitException;
import cat.gencat.clt.git.backend.crosscutting.GitReason;
import cat.gencat.clt.git.backend.mapper.PersistencyMapper;
import cat.gencat.clt.git.backend.model.presenter.PageableResponseModel;
import cat.gencat.clt.git.backend.model.presenter.QdCaCPresenter;
import cat.gencat.clt.git.backend.model.presenter.QdCaCSearchFormModel;
import cat.gencat.clt.git.backend.service.QdCaCService;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/qdcac")
@RequiredArgsConstructor
public class QdCaCController {

    private final PersistencyMapper persistencyMapper;
    private final QdCaCService qdcacService;

    @GetMapping
    public Flux<QdCaCPresenter> all() {
        return this.qdcacService.getQdCaCs();
    }
    
    @GetMapping ("/search")
    public Mono<PageableResponseModel<QdCaCPresenter>> getQdCaCSearch(@Valid QdCaCSearchFormModel form) {
        return this.qdcacService.getQdCaCSearch(form);
    }

    @PostMapping ("/alta")
    public Mono<QdCaCPresenter> save(@RequestBody @Valid QdCaCPresenter qdcac) {
    	return this.qdcacService.store(
            this.persistencyMapper.toPersistency(qdcac)
        );
    }

    @PostMapping(path = "exception")
    public Mono<QdCaCPresenter> exception() {
        return Mono.error(
            GitException.builder()
                .reason(GitReason.QCDF_NOT_UPDATABLE)
                .parameters(new String[]{"any_id"})
                .build()
            );
    }
    
    @PutMapping ("/baixa")
    public Mono<QdCaCPresenter> baixaQdCaC(@RequestBody QdCaCPresenter qdcac) {
    	return this.qdcacService.baixaQdCaC(qdcac.getId(), qdcac.getDataBaixa());
    } 
    
    @GetMapping("/{id}")
    public Mono<QdCaCPresenter> getQdCaCById(@PathVariable Long id) {
        return this.qdcacService.getQdCaCById(id);
    }
    
    @GetMapping("/search/codi")
    public Mono<QdCaCPresenter> getQdCaCbyCodi(@RequestParam(value = "codi") String codi) {
    	return this.qdcacService.getQdCaCbyCodi(codi);
    }
    
    @PutMapping("/edicio")
    public Mono<QdCaCPresenter> update(@RequestBody QdCaCPresenter qdcac) {
        return this.qdcacService.updateQdCaC(
        		this.persistencyMapper.toPersistency(qdcac)
        );
    }
    
//    @GetMapping("/search")
//    public Flux<QdCaC> search(@RequestBody QdCaC qdcacFilters) {
//        return this.qdcacService.searchQdCaCs(qdcacFilters);
//    }
}
