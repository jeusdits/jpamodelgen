package cat.gencat.clt.git.backend.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ServerWebInputException;

import cat.gencat.clt.git.backend.crosscutting.GitException;
import cat.gencat.clt.git.backend.crosscutting.GitReasonCode;
import cat.gencat.clt.git.backend.mapper.PolicyMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
@RequiredArgsConstructor
class RestExceptionHandler {

    private final PolicyMapper policyMapper;

    @ExceptionHandler(GitException.class)
    ResponseEntity<ApiError> postNotFound(GitException ex) {
        log.debug("handling exception: " + ex);

        ApiError apiError = this.policyMapper.toApiError(ex);
        log.error(apiError.getDescription(), ex);

        return ResponseEntity
            .status(this.toHttpStatus(ex.getCode()))
            .body(apiError);
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<ApiError> handleException(WebExchangeBindException ex) {
        log.debug("handling exception: " + ex);

        ApiError apiError = this.policyMapper.toApiError(ex);
        log.error(apiError.getDescription(), ex);

        return ResponseEntity.badRequest().body(apiError);
    }

    @ExceptionHandler(value = ServerWebInputException.class)
    public ResponseEntity<ApiError> errorHandler(ServerWebInputException exception) {
        log.error("ServerWebInputException ", exception);
        return ResponseEntity.badRequest().body(ApiError.builder().build());
    }

    // @ExceptionHandler(value = {ConstraintViolationException.class})
    // public ResponseEntity<String> methodArgumentNotValidException(ConstraintViolationException constraintViolationException){
    //     return ResponseEntity.badRequest().body(constraintViolationException.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(";")));
    // }

    @ExceptionHandler(Exception.class)
    ResponseEntity<ApiError> postNotFound(Exception ex) {
        log.debug("handling exception: " + ex);

        ApiError apiError = this.policyMapper.toApiError(ex);
        log.error(apiError.getDescription(), ex);

        return ResponseEntity
            .status(this.toHttpStatus(GitReasonCode.INTERNAL))
            .body(apiError);
    }

    private HttpStatus toHttpStatus(GitReasonCode gitReasonCode) {
        switch (gitReasonCode) {
            case VALIDATION_FAILED:
                return HttpStatus.BAD_REQUEST;
            case QCDF_NOT_UPDATABLE:
                return HttpStatus.NOT_FOUND;
            case INTERNAL:
            default:
                return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

}