package cat.gencat.clt.git.backend.repositories;

import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.QdCaC;
import reactor.core.publisher.Mono;

@Repository
public interface QdCaCRepository extends ReactiveCrudRepository<QdCaC, Long>, ReactiveQueryByExampleExecutor<QdCaC> {

	Mono<QdCaC> findByCodi(final String codi);
	
}

