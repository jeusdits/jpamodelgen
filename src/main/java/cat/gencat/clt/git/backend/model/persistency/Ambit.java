package cat.gencat.clt.git.backend.model.persistency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "GITTBAMBIT")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBAMBIT")
public class Ambit {

	@Id
	@org.springframework.data.annotation.Id
	@Column(name = "AMBIT_ID")
	@org.springframework.data.relational.core.mapping.Column("AMBIT_ID")
	private String id;
	
	@Column(name = "AMBIT_CODI")
	@org.springframework.data.relational.core.mapping.Column("AMBIT_CODI")
    private String codi;
	
    @Column(name = "AMBIT_DENOMINACIO")
	@org.springframework.data.relational.core.mapping.Column("AMBIT_DENOMINACIO")
    private String nom;
    
}
