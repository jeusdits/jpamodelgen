package cat.gencat.clt.git.backend.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.QdCF;
import io.smallrye.mutiny.Uni;

@Repository
public interface QdCFCriteriaRepository {

    Uni<Page<QdCF>> findByDataBaixaNull(Pageable pageable);
    Uni<Long> getLastCode(String ambit);
    
}
