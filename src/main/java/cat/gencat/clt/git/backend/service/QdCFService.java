package cat.gencat.clt.git.backend.service;

import java.time.LocalDate;
import java.util.Objects;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cat.gencat.clt.git.backend.mapper.PresenterMapper;
import cat.gencat.clt.git.backend.model.persistency.QdCF;
import cat.gencat.clt.git.backend.model.presenter.PageableResponseModel;
import cat.gencat.clt.git.backend.model.presenter.QdCFPresenter;
import cat.gencat.clt.git.backend.model.presenter.QdCFSearchFormModel;
import cat.gencat.clt.git.backend.repositories.QdCFRepository;
import io.smallrye.mutiny.Uni;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class QdCFService {

	private final PresenterMapper presenterMapper;
	private final QdCFRepository qdcfRepository;

	public Uni<Page<QdCFPresenter>> getQdCFs(Pageable pageable) {
		return this.qdcfRepository.findByDataBaixaNull(pageable)
			.map(page -> page.map(this.presenterMapper::toPresenter));
	}
	
	// public Mono<PageableResponseModel<QdCFPresenter>> getQdCFSearchQueryDsl(QdCFSearchFormModel form, Pageable pageable) {
	// 	Function<List<QdCFPresenter>, PageableResponseModel<QdCFPresenter>> toResponse =
	// 		(items) -> PageableResponseModel.<QdCFPresenter>builder().size(items.size()).page(form.getPage()).results(items).build();

	// 	return this.qdcfRepository.findAll(this.queryMapper.toPredicate(form))
	// 		.map(this.presenterMapper::toPresenter)
	// 		.collectList()
	// 		.map(toResponse);
	// }

	public Mono<PageableResponseModel<QdCFPresenter>> getQdCFSearch(QdCFSearchFormModel form) {
	
		QdCF qdcf = new QdCF();
		
		qdcf.setCodi(form.getCodi());
		qdcf.setJerarquic01(form.getJerarquic01());
		qdcf.setJerarquic02(form.getJerarquic02());
		qdcf.setJerarquic03(form.getJerarquic03());
		qdcf.setJerarquic04(form.getJerarquic04());
		qdcf.setJerarquic05(form.getJerarquic05());
		qdcf.setAmbit(form.getAmbit());
		qdcf.setSerie(form.getSerie());
		qdcf.setNivell(form.getNivell());
		qdcf.setNom(form.getNom());
		qdcf.setEstat(form.getEstat());
		qdcf.setPotestat(form.getPotestat());
		qdcf.setPromotor(form.getPromotor());
		
		ExampleMatcher matcher = ExampleMatcher.matchingAny().withIgnoreCase();
		
		// TODO: mostrarbaixes == 0 --> camps dataBaixa is null / mostrarbaixes == 1 --> camps dataBaixa is not null 
		if (form.getMostrarbaixes() == 0) {
//			matcher.withIgnorePaths("dataBaixa").withIgnoreNullValues();
			matcher.withMatcher("dataBaixa", ExampleMatcher.GenericPropertyMatchers.contains());
		}			
		
		Example<QdCF> example = Example.of(qdcf, matcher);
		
		// Page<Member> members2 = memberRepository.findAll(example, new PageRequest(0, 10, Sort.Direction.DESC, "id"));
		Flux<QdCFPresenter> qdcfs = this.qdcfRepository.findAll(example)
				.map(this.presenterMapper::toPresenter);
		
		Mono<PageableResponseModel<QdCFPresenter>> response = qdcfs.collectList()
				.map(items -> 
					new PageableResponseModel<QdCFPresenter>(Long.valueOf(items.size()), 1, 10, items)
				);
		
		return response;
		
	}


	public Mono<QdCFPresenter> store(QdCF qdcf) {
		return this.qdcfRepository.save(qdcf)
			.map(this.presenterMapper::toPresenter);
		/*if (customerNew != null) {
			return customerRepository.save(customerNew);
		}
		return new Customer();*/
	}
	
	public Uni<Long> getQdCFlastCodi(String ambit) {
		return this.qdcfRepository.getLastCode(ambit);
	}
	

	public Mono<QdCFPresenter> getQdCFById(Long id) {
		return this.qdcfRepository.findById(id)
			.map(this.presenterMapper::toPresenter);
	}
	
	public Mono<QdCFPresenter> getQdCFbyCodi(String codiAmbit) {
		Long codi = Long.parseLong(codiAmbit.substring(2));
		String ambit = codiAmbit.substring(0, 2);
		return this.qdcfRepository.findByCodiAndAmbit(codi, ambit)
			.map(this.presenterMapper::toPresenter);
	}
	
	public Mono<QdCFPresenter> baixaQdCF(Long id, LocalDate dataBaixa) {
		Mono<QdCF> qdcf = this.qdcfRepository.findById(id);
		qdcf.subscribe(item -> {
			item.setDataBaixa(dataBaixa);
			updateQdCF(item);
		}, error -> System.err.println("Error: " + error));
		
		return qdcf.map(this.presenterMapper::toPresenter);
		
	}
	
	public Mono<QdCFPresenter> updateQdCF(QdCF qdcfFiltres) {
		final Mono<QdCFPresenter> qdcf = getQdCFById(qdcfFiltres.getId());
		if (Objects.isNull(qdcf)) {
			   return Mono.empty();
		}
		return this.qdcfRepository.save(qdcfFiltres).map(this.presenterMapper::toPresenter);
		/*
		 * Long num = customerUpdated.getId();
		if (customerRepository.findById(num).isPresent()) {
			Customer customerToUpdate = new Customer();
			customerToUpdate.setId(customerUpdated.getId());
			customerToUpdate.setName(customerUpdated.getName());
			customerToUpdate.setSurname(customerUpdated.getSurname());
			customerToUpdate.setBirtdate(customerUpdated.getBirtdate());
			customerToUpdate.setPhone(customerUpdated.getPhone());
			customerToUpdate.setCountry(customerUpdated.getCountry());
			customerToUpdate.setCity(customerUpdated.getCity());
			customerToUpdate.setDirection(customerUpdated.getDirection());
			customerToUpdate.setPostCode(customerUpdated.getPostCode());
			customerRepository.save(customerToUpdate);
			return "Customer modificado";
		}
		return "Error al modificar el Customer";
		 */
	}
	
//	public Mono<QdCFPresenter> delete(final Long id) {
//		  final Mono<QdCFPresenter> dbStudent = getQdCFById(id);
//		  if (Objects.isNull(dbStudent)) {
//		   return Mono.empty();
//		  }
//		  return getQdCFById(id)
//				  .switchIfEmpty(Mono.empty())
//				  .filter(Objects::nonNull)
//				  .flatMap(qdcfToBeDeleted -> this.qdcfRepository.delete(qdcfToBeDeleted).then(Mono.just(qdcfToBeDeleted)));
//	
//	
//	/*if (customerRepository.findById(id).isPresent()) {
//			customerRepository.deleteById(id);
//			return "Customer eliminado correctamente.";
//		}
//		return "Error! El customer no existe";*/
//	}
//	
//	public Flux<cat.gencat.clt.git.backend.model.presenter.QdCF> searchQdCFs(QdCF qdcfFilters) {
//		return this.qdcfRepository.findQdCFByFilters(qdcfFilters)
//			.map(this.presenterMapper::toPresenter);
//	}
    
}
