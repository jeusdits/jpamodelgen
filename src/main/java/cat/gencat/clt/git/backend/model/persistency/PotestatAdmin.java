package cat.gencat.clt.git.backend.model.persistency;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "GITTBPOTESTATADM")
@org.springframework.data.relational.core.mapping.Table(name = "GITTBPOTESTATADM")
public class PotestatAdmin {

	@Id
	@org.springframework.data.annotation.Id
	@Column(name = "POTESTATADM_ID")
	@org.springframework.data.relational.core.mapping.Column("POTESTATADM_ID")
	private Long id;
	
	@Column(name = "POTESTATADM_DENOMINACIO")
	@org.springframework.data.relational.core.mapping.Column("POTESTATADM_DENOMINACIO")
    private String nom;
	
}
