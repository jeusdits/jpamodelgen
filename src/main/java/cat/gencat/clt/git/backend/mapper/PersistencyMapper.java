package cat.gencat.clt.git.backend.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import cat.gencat.clt.git.backend.model.persistency.QdCF;
import cat.gencat.clt.git.backend.model.persistency.QdCaC;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface PersistencyMapper {
    
    QdCF toPersistency(cat.gencat.clt.git.backend.model.presenter.QdCFPresenter qdcf);
    QdCaC toPersistency(cat.gencat.clt.git.backend.model.presenter.QdCaCPresenter qdcac);

}
