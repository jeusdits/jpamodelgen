package cat.gencat.clt.git.backend.repositories;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.Promotor;

@Repository
public interface PromotorRepository extends ReactiveCrudRepository<Promotor, Long>{

}
