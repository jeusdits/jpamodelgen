package cat.gencat.clt.git.backend.repositories;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import cat.gencat.clt.git.backend.model.persistency.Ambit;

@Repository
public interface AmbitRepository extends ReactiveCrudRepository<Ambit, Long>{

}
