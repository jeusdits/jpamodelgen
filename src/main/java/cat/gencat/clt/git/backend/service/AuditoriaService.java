package cat.gencat.clt.git.backend.service;

import org.springframework.stereotype.Service;

import cat.gencat.clt.git.backend.mapper.PresenterMapper;
import cat.gencat.clt.git.backend.model.presenter.AuditoriaPresenter;
import cat.gencat.clt.git.backend.model.presenter.AuditoriaSearchFormModel;
import cat.gencat.clt.git.backend.model.presenter.PageableResponseModel;
import cat.gencat.clt.git.backend.repositories.AuditoriaRepository;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
@Service
public class AuditoriaService {

	private final PresenterMapper presenterMapper;
	private final AuditoriaRepository auditoriaRepository;
	
	public  Flux<AuditoriaPresenter> getAuditories() {
		return this.auditoriaRepository.findAll()
			.map(this.presenterMapper::toPresenter);
	}
	
	public Mono<PageableResponseModel<AuditoriaPresenter>> getAuditSearch(AuditoriaSearchFormModel form) {
		Flux<AuditoriaPresenter> auditResp = this.auditoriaRepository.findAll()
				.map(this.presenterMapper::toPresenter);
		Mono<PageableResponseModel<AuditoriaPresenter>> response = auditResp.collectList()
				.map(items -> 
					new PageableResponseModel<AuditoriaPresenter>(Long.valueOf(items.size()), 1, 10, items)
				);
		
		return response;
	}
	
}
