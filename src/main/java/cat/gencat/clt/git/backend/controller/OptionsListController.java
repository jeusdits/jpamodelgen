package cat.gencat.clt.git.backend.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cat.gencat.clt.git.backend.model.presenter.AccioAuditPresenter;
import cat.gencat.clt.git.backend.model.presenter.Ambit;
import cat.gencat.clt.git.backend.model.presenter.CodiSIA;
import cat.gencat.clt.git.backend.model.presenter.Instrument;
import cat.gencat.clt.git.backend.model.presenter.PotestatAdmin;
import cat.gencat.clt.git.backend.model.presenter.Promotor;
import cat.gencat.clt.git.backend.model.presenter.TaadPresenter;
import io.smallrye.mutiny.Uni;
import reactor.core.publisher.Flux;

@RequestMapping(value = "/options")
public interface OptionsListController {

	@GetMapping("/ambits")
	public Flux<Ambit> getAmbitsList();
	
	@GetMapping("/instruments")
	public Flux<Instrument> getInstrumentsList();
	
	@GetMapping("/accionsaudits")
	public Uni<List<AccioAuditPresenter>> getAccionsAuditsList();
	
	@GetMapping("/potestatadmin")
	public Flux<PotestatAdmin> getPotestatAdminList();
	
	@GetMapping("/promotors")
	public Flux<Promotor> getPromotorsList();
	
	@GetMapping("/codiSIA")
	public Flux<CodiSIA> getCodiSIAList();
	
	@GetMapping("/taads")
	public Flux<TaadPresenter> getTAADsList();
	

	
}
