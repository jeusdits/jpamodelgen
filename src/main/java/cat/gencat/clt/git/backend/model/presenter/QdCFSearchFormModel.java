package cat.gencat.clt.git.backend.model.presenter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class QdCFSearchFormModel extends Pageable {
	
	private Long codi;
	private String jerarquic01;
	private String jerarquic02;
	private String jerarquic03;
	private String jerarquic04;
	private String jerarquic05;
	private String ambit;
	private Long serie;
	private Long nivell;
	private String nom;
	private Long estat;
	private Long potestat;
	private Long promotor;
	private int mostrarbaixes;
	
}
