package cat.gencat.clt.git.backend.configuration;

import java.net.URI;
import java.security.Key;

import javax.persistence.Persistence;

import org.hibernate.reactive.mutiny.Mutiny;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;

// import com.querydsl.sql.SQLServer2012Templates;
// import com.querydsl.sql.SQLServerTemplates;
// import com.querydsl.sql.SQLTemplates;

import io.jsonwebtoken.security.Keys;

@Configuration
@EnableConfigurationProperties(GitProperties.class)
public class GitConfiguration {

    @Bean
    public URI redirectionURI(GitProperties gitProperties) {
        return UriComponentsBuilder.newInstance()
            .scheme("http")
            .host(gitProperties.getLoginService().getUrlUi())
            .port(gitProperties.getLoginService().getPortUi())
            .path(gitProperties.getLoginService().getPathUi())
            .build()
            .toUri();
    }

    @Bean
    public Key jwtSharedKey(GitProperties gitProperties) {
        return Keys.hmacShaKeyFor(gitProperties.getTokenService().getSecret().getBytes());
    }

    // @Bean
    // public SQLTemplates sqlTemplates(){
    //     return SQLServer2012Templates.DEFAULT;
    // }

    @Bean
    public Mutiny.SessionFactory sessionFactory() {
        return Persistence.createEntityManagerFactory("blogPU")
            .unwrap(Mutiny.SessionFactory.class);
    }


}
