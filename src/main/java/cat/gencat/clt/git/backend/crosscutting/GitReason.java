package cat.gencat.clt.git.backend.crosscutting;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GitReason {
    
    VALIDATION_FAILED(GitResource.SERVICE, GitReasonCode.VALIDATION_FAILED, "Validation failed"),
    QCDF_NOT_UPDATABLE(GitResource.QCDF, GitReasonCode.QCDF_NOT_UPDATABLE, "QCDF with id '%s' doesn't exists");

    private final GitResource gitResource;
    private final GitReasonCode code;
    private final String message;

}
