package cat.gencat.clt.git.backend.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cat.gencat.clt.git.backend.model.presenter.AuditoriaSearchFormModel;
import cat.gencat.clt.git.backend.model.presenter.AuditoriaPresenter;
import cat.gencat.clt.git.backend.model.presenter.PageableResponseModel;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping(value = "/audits")
public interface AuditoriaController {
	
	@GetMapping
    public Flux<AuditoriaPresenter> all();

	@GetMapping("/search")
	public Mono<PageableResponseModel<AuditoriaPresenter>> getAuditories(@Valid AuditoriaSearchFormModel form);
	
}
