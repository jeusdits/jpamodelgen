package cat.gencat.clt.git.backend.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import cat.gencat.clt.git.backend.model.persistency.AccioAudit;
import cat.gencat.clt.git.backend.model.persistency.Auditoria;
import cat.gencat.clt.git.backend.model.presenter.AccioAuditPresenter;
import cat.gencat.clt.git.backend.model.presenter.Ambit;
import cat.gencat.clt.git.backend.model.presenter.AuditoriaPresenter;
import cat.gencat.clt.git.backend.model.presenter.CodiSIA;
import cat.gencat.clt.git.backend.model.presenter.Instrument;
import cat.gencat.clt.git.backend.model.presenter.PotestatAdmin;
import cat.gencat.clt.git.backend.model.presenter.Promotor;
import cat.gencat.clt.git.backend.model.presenter.QdCFPresenter;
import cat.gencat.clt.git.backend.model.presenter.QdCaCPresenter;
import cat.gencat.clt.git.backend.model.presenter.TaadPresenter;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface PresenterMapper {
    
    Ambit toPresenter(cat.gencat.clt.git.backend.model.persistency.Ambit ambit);
    Instrument toPresenter(cat.gencat.clt.git.backend.model.persistency.Instrument instrument);
    AccioAuditPresenter toPresenter(AccioAudit accioAudit);
    List<AccioAuditPresenter> toPresenter(List<AccioAudit> accioAudit);
    PotestatAdmin toPresenter(cat.gencat.clt.git.backend.model.persistency.PotestatAdmin potestatAdmin);
    Promotor toPresenter(cat.gencat.clt.git.backend.model.persistency.Promotor promotor);
    CodiSIA toPresenter(cat.gencat.clt.git.backend.model.persistency.CodiSIA codiSIA);
    TaadPresenter toPresenter(cat.gencat.clt.git.backend.model.persistency.Taad taad);
    
    
    QdCFPresenter toPresenter(cat.gencat.clt.git.backend.model.persistency.QdCF qdcf);
    QdCaCPresenter toPresenter(cat.gencat.clt.git.backend.model.persistency.QdCaC qdcac);
    
    AuditoriaPresenter toPresenter(Auditoria auditoria);

}
