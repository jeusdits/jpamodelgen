package cat.gencat.clt.git.backend.model.presenter;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class QdCaCPresenter {

    private Long id;

    @NotNull
    private String codi;
    
	private String jerarquic01;
	
	private String jerarquic02;
	
	private String jerarquic03;

	private Long nivell;
		
	private Long vigencia;
	
	private Long generic;
	
	private String nom;
	
	private String titol;
	
	private String altresNoms;
	
	private Long representa;
	
	private String descripcio;
	
	private Long serie;
	
	private Long potestat;
	
	private Long promotor;
	
	private String funcioadm;
	
	private String seriesrel;
	
	private String documents;
	
	private String marclegal;
	
	private Long taad;
		
	private String nomproces;
	
	private Long sia;
	
	private LocalDate dataAlta;
	
	private LocalDate dataBaixa;
	
	private String observacions;
	
	private Long estat;
	
	private LocalDate pritraPublicacio;
	
	private LocalDate dartraPublicacio;
	
	private Long pareid;
    
}
